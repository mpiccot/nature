import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
 //Servlet for getting a student table when you are an admin
public class AdminRequest extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      try {
         //connect to the DB, and get students based on the session id
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         String gquery = "select * from student where sessionID=" + request.getParameter("sessionid");//request.getParameter("sessionid");
         ResultSet rset = stmt.executeQuery(gquery);
         //set variables used later for editing and delete
         //also make the ajax request that does a delete request for a row using delreq.java
         //it takes in data from the selected row in the table
         out.println("<script type='text/javascript' charset='utf-8'>");
         out.println("  var oTable;");
         out.println("  var giRedraw = false;");
         out.println("  var nEditing = false");
         out.println("  var rowedit = null");
         out.println("function setDB(){");
         out.println("  var dat = getData(oTable);");
         out.println("  $.ajax({");
         out.println("    url:'delreq',");
         out.println("    data: {data: dat.toString()},");
         out.println("    type:'GET',");
         out.println("     success: function(data) {");
         out.println("             $('#ajax_result3').html(data);");
         out.println("         },");
         out.println("         error: function(xhr, status, error) {");
         out.println("           $('#ajax_result3').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
         out.println("         }");
         out.println("  })");
         out.println("}");
      //ajax for saving a row after the html was edited, it takes in the type of edit (in this case a student edit) and data from the selected row that was set
      //the servlet editreq.java knows what studentid to use, because the studentid can not change
         out.println("function editDB(){");
         out.println("  var dat = oTable._(rowedit);");
         out.println("  $.ajax({");
         out.println("    url:'editreq',");
         out.println("    data: {data: dat.toString(), type: 'student'},");
         out.println("    type:'GET',");
         out.println("     success: function(data) {");
         out.println("             $('#ajax_result4').html(data);");
         out.println("         },");
         out.println("         error: function(xhr, status, error) {");
         out.println("           $('#ajax_result4').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
         out.println("         }");
         out.println("  })");
         out.println("}");
         out.println("  $(document).ready(function() {");
            

               // Add a click handler to the rows, this is how the row is selected
         out.println("     $('#students_table tbody').click(function(event) {");
         out.println("        $(oTable.fnSettings().aoData).each(function (){");
         out.println("           $(this.nTr).removeClass('row_selected');");
         out.println("        });");
         out.println("        $(event.target.parentNode).addClass('row_selected');");
         out.println("     });");
               //function for the delete button
         //if you are in edit mode, it autosaves your edit, then calls the delete ajax for the database, then deletes the row visually in the html table
         out.println("     $('#delete').click( function() {");
         out.println("        var anSelected = fnGetSelected( oTable );");
         out.println("        if (nEditing == true){");
         out.println("        $('#edit').click();");
         out.println("        }");
         out.println("        setDB();");
         out.println("        oTable.fnDeleteRow( anSelected[0] );");
         out.println("     } );");
         //edit function for the edit button
         //if you are not in edit mode, call editrow on the currently selected row, and change edit button to save
         //if you are in edit mode, call saverow on the edited row, then execute the editDB ajax save, and change save button back to edit
         out.println("$('#edit').click( function () {");
         out.println("        var anSelected = fnGetSelected( oTable );");   
         out.println("if(nEditing == false && anSelected[0] !== null){");
         out.println("editRow( oTable, anSelected[0]);");
         out.println("nEditing = true;");
         out.println("rowedit = anSelected[0]");
         out.println("document.getElementById(\"edit\").value=\"Save Modified Student\";");
         out.println("}");
         out.println("else if (nEditing == true){");
         out.println("saveRow( oTable, rowedit );");
         out.println("editDB();");
         out.println("document.getElementById(\"edit\").value=\"Edit Selected Student\";");
         out.println("nEditing = false;");
         out.println("}");
         out.println("} );");
               // Init the datatable 
         out.println("     oTable = $('#students_table').dataTable({");
         out.println("           'bPaginate': false,");                
         out.println("\"sDom\": 'T<\"clear\">r<\"H\"lf><\"datatable-scroll\"t><\"F\"ip>',");
         out.println("\"oTableTools\": {");
         out.println("\"sSwfPath\": \"./libraries/TableTools-2.1.5/media/swf/copy_csv_xls_pdf.swf\"");
         out.println("},");
         out.println("           'bPaginate': true,");
         out.println("           'bRetrieve': true");
         out.println("  } );");
         out.println("} );");
         
         // Gets the rows which are currently selected, and returns them
         out.println("  function fnGetSelected( oTableLocal )");
         out.println("  {");
         out.println("     var aReturn = new Array();");
         out.println("     var aTrs = oTableLocal.fnGetNodes();");
         
         out.println("     for ( var i=0 ; i<aTrs.length ; i++ )");
         out.println("     {");
         out.println("        if ( $(aTrs[i]).hasClass('row_selected') )");
         out.println("        {");
         out.println("           aReturn.push( aTrs[i] );");
         out.println("        }");
         out.println("     }");
         out.println("     return aReturn;");
         out.println("  }");
         //get datafunction that gets the row data from the selected row in the table
         out.println("  function getData( oTableLocal )");
         out.println("  {");
         out.println("     var data = oTableLocal._(fnGetSelected(oTableLocal))[0];");
         out.println("     return data[0];");
         out.println("  }");

         //saverow updates the html in the table with the new edited values

         out.println("function saveRow ( oTableLocal, nRow )");
         out.println("{");
         out.println("var jqInputs = $('input', nRow);");
         out.println("oTableLocal.fnUpdate( jqInputs[0].value, nRow, 1, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[1].value, nRow, 2, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[2].value, nRow, 3, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[3].value, nRow, 4, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[4].value, nRow, 5, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[5].value, nRow, 6, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[6].value, nRow, 7, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[7].value, nRow, 8, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[8].value, nRow, 9, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[9].value, nRow, 10, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[10].value, nRow, 11, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[11].value, nRow, 12, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[12].value, nRow, 13, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[13].value, nRow, 14, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[14].value, nRow, 15, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[15].value, nRow, 16, false );");
         out.println("oTableLocal.fnUpdate( jqInputs[16].value, nRow, 17, false );");
         out.println("oTableLocal.fnDraw();");
         out.println("}");
         //editrow changes each value in the selected row with a textbox with the initial value set to be the value before editing
         out.println("function editRow ( oTableLocal, nRow )");
         out.println("{");
         out.println("var aData = oTableLocal.fnGetData(nRow);");
         out.println("var jqTds = $('>td', nRow);");
         out.println(" jqTds[1].innerHTML = '<input type=\"text\" value=\"'+aData[1]+'\" id=\"sbox1\" maxlength=\"20\">';");
         out.println(" jqTds[2].innerHTML = '<input type=\"text\" value=\"'+aData[2]+'\" id=\"sbox2\" maxlength=\"20\">';");
         out.println(" jqTds[3].innerHTML = '<input type=\"text\" value=\"'+aData[3]+'\" id=\"sbox3\" maxlength=\"7\">';");
         out.println(" jqTds[4].innerHTML = '<input type=\"text\" value=\"'+aData[4]+'\" id=\"sbox4\" maxlength=\"20\">';");
         out.println(" jqTds[5].innerHTML = '<input type=\"text\" value=\"'+aData[5]+'\" id=\"sbox5\" maxlength=\"20\">';");
         out.println(" jqTds[6].innerHTML = '<input type=\"text\" value=\"'+aData[6]+'\" id=\"sbox6\" maxlength=\"20\">';");
         out.println(" jqTds[7].innerHTML = '<input type=\"text\" value=\"'+aData[7]+'\" id=\"sbox7\" maxlength=\"20\">';");
         out.println(" jqTds[8].innerHTML = '<input type=\"text\" value=\"'+aData[8]+'\" id=\"sbox8\" maxlength=\"20\">';");
         out.println(" jqTds[9].innerHTML = '<input type=\"text\" value=\"'+aData[9]+'\" id=\"sbox9\" maxlength=\"20\">';");
         out.println(" jqTds[10].innerHTML = '<input type=\"text\" value=\"'+aData[10]+'\" id=\"sbox10\" maxlength=\"20\">';");
         out.println(" jqTds[11].innerHTML = '<input type=\"text\" value=\"'+aData[11]+'\" id=\"sbox11\" maxlength=\"20\">';");
         out.println(" jqTds[12].innerHTML = '<input type=\"text\" value=\"'+aData[12]+'\" id=\"sbox12\" maxlength=\"20\">';");
         out.println(" jqTds[13].innerHTML = '<input type=\"text\" value=\"'+aData[13]+'\" id=\"sbox13\" maxlength=\"100\">';");
         out.println(" jqTds[14].innerHTML = '<input type=\"text\" value=\"'+aData[14]+'\" id=\"sbox14\" maxlength=\"20\">';");
         out.println(" jqTds[15].innerHTML = '<input type=\"text\" value=\"'+aData[15]+'\" id=\"sbox15\" maxlength=\"20\">';");
         out.println(" jqTds[16].innerHTML = '<input type=\"text\" value=\"'+aData[16]+'\" id=\"sbox16\" maxlength=\"20\">';");
         out.println(" jqTds[17].innerHTML = '<input type=\"text\" value=\"'+aData[17]+'\" id=\"sbox17\" maxlength=\"20\">';");
         //alphanum extensions to limit what you can edit in these rows
         out.println("  $(\"#sbox1\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox2\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox3\").alphanum({allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox4\").alphanum({allow: '.', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox5\").alphanum({allow: '.', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox6\").alphanum({allow: '.', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox7\").alphanum({allow: '.', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox8\").alphanum({allow: '.', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox9\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox10\").alphanum({allow: '.', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox11\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox12\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      
      out.println("  $(\"#sbox13\").alphanum({allow: ';:', allowOtherCharSets : false});");
      out.println("  $(\"#sbox14\").alphanum({allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox15\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox16\").alphanum({allowNumeric: false, allowOtherCharSets : false});");
      out.println("  $(\"#sbox17\").alphanum({allow: '.%', allowLatin: false, allowOtherCharSets : false});");
         out.println("}");
         out.println("</script>");
      
       //make the student table using a loop
         if (rset.next()){
            out.println("<h2>SessionID: " + rset.getString("sessionid") + "</h2>");
            out.println("<table id='students_table' border='1' class='display'><thead><tr><th>Student ID</th><th>Last Name</th><th>First Name</th><th>Cubic Meter</th><th>Temperature</th><th>Air Pressure</th><th>Humidity</th><th>Wind Speed</th><th>Rain Meter</th><th>Current Precipitation</th><th>Altitude</th><th>Cloud Type</th><th>Cloud Cover</th><th>Insects</th><th>Soil pH</th><th>Soil Color</th><th>Soil Type</th><th>Canopy Cover</th></tr></thead><tbody>");
            rset.previous();
         while(rset.next()){
            out.println("<tr><td>" + rset.getString("ID") + "</td><td>" + rset.getString("lName") + "</td><td>" + rset.getString("fName") + "</td>");
            out.println("<td>" + rset.getString("cubicNum") + "</td><td>" + rset.getString("temperature") + "</td><td>" + rset.getString("aPressure") + "</td>");
            out.println("<td>" + rset.getString("humidity") + "</td><td>" + rset.getString("wSpeed") + "</td><td>" + rset.getString("rMeter") + "</td>");
            out.println("<td>" + rset.getString("cPrecipitation") + "</td><td>" + rset.getString("altitude") + "</td><td>" + rset.getString("cType") + "</td>");
            out.println("<td>" + rset.getString("cCover") + "</td><td>" + rset.getString("insects") + "</td><td>" + rset.getString("sPH") + "</td>");
            out.println("<td>" + rset.getString("sColor") + "</td><td>" + rset.getString("sType") + "</td><td>" + rset.getString("canopyCover") + "</td>");
            out.println("</tr>");
            
         }
		   out.println("</tbody></table><br>");
         out.println("<input type='button' class='btn btn-primary' value='Delete Selected Student' name='del_stu' id='delete'>");
         out.println("<input type='button' class='btn btn-primary' value='Edit Selected Student' name='edit_stu' id='edit'>");
      }
      else{
         //if no records, report it
         out.println("No such sessionID found/no records for this session");
      }
      
         out.println("<div id='ajax_result4'></div>");
      } 
      catch (Exception e) 
         {
            //this should be caught before you can type it, but just in case...
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
            out.println("Invalid entry, only use the number 0-9 for your session ID");
         } 
         finally 
         {
            out.close();
            try 
            {
               if (conn != null) 
               {
                  conn.close();
               }
            } 
            catch (SQLException e)
            {
            }               
         }
   }
}