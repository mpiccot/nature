import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
 //Servlet to delete a row in the DB
public class deletereq extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      String firstName = "";
      try {
         //connect to DB
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         //make string and execute update by deleting row on the studentid
         String gquery = "delete from student where ID=" + request.getParameter("data");
         stmt.executeUpdate(gquery);
      } 
      catch (Exception e) 
         {
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
         } 
         finally 
         {
            out.close();
            try 
            {
               if (conn != null) 
               {
                  conn.close();
               }
            } 
            catch (SQLException e)
            {
            }               
         }
   }
}