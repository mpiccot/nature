import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//Servlet for the e-mail request, sending an e-mail
public class EmailRequest extends HttpServlet {
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      PrintWriter out = response.getWriter();

      try {
        //get the parameters
	  String fname = request.getParameter("fname");
	  String lname = request.getParameter("lname");
	  String email = request.getParameter("email");
	  String subject = request.getParameter("subject");
	  String user_message = request.getParameter("message");
	  
      //set the login info for the sending e-mail account
        final String username = "charlottenaturemuseum@gmail.com";
        final String password = "cubicmeter";
        //set the properties of the e-mail account
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });

        try {
          //send mail to the same address
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("charlottenaturemuseum@gmail.com"));
            //add in proper parameters from the page
            message.setSubject(fname+" "+lname+" sent an email - "+subject);
            message.setText(user_message+"\n\n"+"Senders E-mail: "+email);
            //transport the mail
            Transport.send(message);

            System.out.println("Email Sent");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	  
	  }catch (Exception ex){
		    ex.printStackTrace();
       System.err.println("Exception: " + ex.getMessage());
	  }finally{
      //redirect back to the main page
		response.sendRedirect("./NatureHome.html");
	  }

	 
	 }
}