import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
 //Servlet to edit an entry in the database
public class editreq extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      String firstName = "";
      try {
         //connect to DB
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         String gquery = null;
         String[] str = new String[18];
         Arrays.fill(str, "");
         int i = 0, j = 0;
         String data = request.getParameter("data");
         //PARSING, parses the data parameter string that is seperated by commas
         for (i = 0; i < data.length(); i++){
               if (data.charAt(i) != ',')
                  str[j] += data.charAt(i);
               else
                  j++;
            }
            //if the edit request is on the student table, create the string properly and execute update
         if (request.getParameter("type").equals("student")){
            gquery = "update student set lName='"+str[1]+"', fName='"+str[2]+"', cubicNum="+Integer.parseInt(str[3])+", temperature="+Double.parseDouble(str[4])+", aPressure="+Double.parseDouble(str[5])+", humidity="+Double.parseDouble(str[6])+", wSpeed="+Double.parseDouble(str[7])+", rMeter="+Double.parseDouble(str[8])+", cPrecipitation='"+str[9]+"', altitude="+Double.parseDouble(str[10])+", cType='"+str[11]+"', cCover='"+str[12]+"', insects='"+str[13]+"', sPH="+Double.parseDouble(str[14])+", sColor='"+str[15]+"', sType='"+str[16]+"', canopyCover='"+str[17]+"' where ID="+str[0];
            stmt.executeUpdate(gquery);

         }
         //if edit request is on session table, create string properly and execute update
         else if (request.getParameter("type").equals("session")){
            gquery = "update discovery_sessions set PROF_FNAME='"+str[2]+"', PROF_LNAME='"+str[1]+"', PROF_EMAIL='"+str[3]+"', PROF_PHONE='"+str[4]+"', SESSION_ACTIVE='"+str[5]+"' where SESSION_NUMBER="+Integer.parseInt(str[0]);
            stmt.executeUpdate(gquery);
         }
      } 
      catch (Exception e) 
         {
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
         } 
         finally 
         {
            out.close();
            try 
            {
               if (conn != null) 
               {
                  conn.close();
               }
            } 
            catch (SQLException e)
            {
            }               
         }
   }
}