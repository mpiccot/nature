import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
//Servlet for adding a session
public class AddRequest extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      try {
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         //parse the update string from parameters
         String str1 = "('" + request.getParameter("fname") + "','" + request.getParameter("lname") + "','";
         String str2 = request.getParameter("email") + "','" + request.getParameter("phone") + "','Y')";
         String gquery = "insert into discovery_sessions (PROF_FNAME,PROF_LNAME,PROF_EMAIL,PROF_PHONE, SESSION_ACTIVE) values" + str1 + str2;
         //execute the update
         stmt.executeUpdate(gquery);
      } 
      catch (Exception e) 
         {
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
         } 
         finally 
         {
            out.close();
            try 
            {
               if (conn != null) 
               {
                  conn.close();
               }
            } 
            catch (SQLException e)
            {
            }               
         }
   }
}