/*
Handle requests made by the mobile application to retrieve session information / store session data
*/
import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
 
public class MobileRequest extends HttpServlet {
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
		 
	  // response.setContentType("application/json");
 
      // Allocate a output writer to write the response message into the network socket
      PrintWriter out = response.getWriter();

      try {
		//get the connection object using our local database connection
		String request_type = request.getParameter("request_type").trim();
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
		//create statement object for our query
		Statement stmt = conn.createStatement();
		
		
		//This servlet handles two types of requests
		//The "SESSIONS" request is for a request requesting session information
		//this segment returns the sessions in html format to the mobile device
		if(request_type.equals("Sessions")){
			
			String query = "SELECT SESSION_NUMBER, PROF_FNAME, PROF_LNAME FROM discovery_sessions WHERE SESSION_ACTIVE = \"Y\"";
			ResultSet rs = stmt.executeQuery(query);
			StringBuilder sb = new StringBuilder();
				sb.append("<option value=\"\" disabled selected >Select Session</option>");
			while(rs.next()){
				sb.append("<option value="+rs.getString("SESSION_NUMBER")+">"+rs.getString("PROF_FNAME")+" "+rs.getString("PROF_LNAME")+"</option>");
			}
			
			out.println(sb.toString());
			
			stmt.close();
			rs.close();
			conn.close();
		//This segment handles a storage request from the mobile application
		//All of the information passed in from the mobile app gets store into the local database
		}else if (request_type.equals("Store")){
		
		//variables used (in order)
			int session = Integer.parseInt(request.getParameter("Session").trim());
			String fname = request.getParameter("Fname").trim();
			String lname = request.getParameter("Lname").trim();
			int cubicnumber = Integer.parseInt(request.getParameter("CubicNumber").trim());
			double temperature = Double.parseDouble(request.getParameter("Temperature").trim());
			double airpressure = Double.parseDouble(request.getParameter("AirPressure").trim());
			double humidity = Double.parseDouble(request.getParameter("Humidity").trim());
			double windspeed = Double.parseDouble(request.getParameter("WindSpeed").trim());
			double rainmeter = Double.parseDouble(request.getParameter("RainMeter").trim());
			String precipitation = request.getParameter("Precipitation").trim();
			double altitude = Double.parseDouble(request.getParameter("Altitude").trim());
			String cloudtype = request.getParameter("CloudType").trim();
			String cloudcover = request.getParameter("CloudCover").trim();
			String insects = request.getParameter("Insects").trim();
			int phnum = Integer.parseInt(request.getParameter("phNum").trim());
			String soilcolor = request.getParameter("SoilColor").trim();
			String soiltype = request.getParameter("SoilType").trim();
			String canopy = request.getParameter("Canopy").trim();
			//calculating the percentage of canopy covered
			String canopyPercent = (canopy.length() * 12.5) + "%";
			
			int totalinsects = Integer.parseInt(request.getParameter("TotalInsects").trim());
			
			/*
			* Build query that will insert mobile submission into database
			*/
			
			String query = "INSERT INTO student (sessionID, lName, fName, cubicNum, temperature, aPressure, humidity, wSpeed, rMeter, cPrecipitation, altitude, cType, cCover, insects, sPH, sColor, sType, canopyCover) VALUES ("+session+", '"+lname+"', '"+fname+"', "+cubicnumber+", "+temperature+", "+airpressure+", "+humidity+", "+windspeed+", "+rainmeter+", '"+precipitation+"', "+altitude+", '"+cloudtype+"', '"+cloudcover+"', '"+insects+"', "+phnum+", '"+soilcolor+"', '"+soiltype+"', '"+canopyPercent+"')";
			
			System.out.println(query);
			//execute the query, this line returns a number of rows changed
			 int rowsChanged = stmt.executeUpdate(query);
			 if(rowsChanged > 0){
				out.println("Request Successful");
			 }
			
			stmt.close();
			conn.close();
			
			
		}
		
		//catch errors
	  }catch (Exception ex){
		    ex.printStackTrace();
			System.err.println("Exception: " + ex.getMessage());
	  }finally{
		out.close();
	  }

	 
	 }//End doPost
	 
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
			doPost(request,response);
		 }
}//End Class