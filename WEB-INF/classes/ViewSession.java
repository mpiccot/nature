import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
//Servlet for directing from the main page
public class ViewSession extends HttpServlet {
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      try {
      //get information from page
	  String session_id = request.getParameter("sess_id");
	  String user_name = request.getParameter("user_name");
	  String password = request.getParameter("password");
	  //if session id is null, then you tried to login as an admin
	  if(session_id == null){
	  	//query the admin database for a matching username
	  	conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         String gquery = "select * from admin where uName='" + user_name + "'";
         ResultSet rset = stmt.executeQuery(gquery);
         //if no matching username, return old page, and state username doesn't exist
         if (!(rset.next())){
         	out.println("<!DOCTYPE HTML>");
         	out.println("<html>");
out.println("  <head>");
out.println("	<title>Main Page</title>");
out.println("	    <script src='./js/jquery.js'></script>");
out.println("		<script src='./libraries/bootstrap/js/bootstrap.min.js'></script>");

out.println("		<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap.min.css'>");
out.println("		<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap-responsive.min.css'>");
out.println("		<script type=\"text/javascript\" language=\"javascript\" src=\"./libraries/AlphaNum/jquery.alphanum.js\"></script>");
out.println("		<script type=\"text/javascript\" charset=\"utf-8\">");
out.println("		$(document).ready(function() {");
out.println("			$(\"#uname\").alphanum({allowOtherCharSets : false});");
out.println("			$(\"#pword\").alphanum({allowOtherCharSets : false});");
out.println("			$(\"#session_id\").alphanum(allowLatin: false, allowOtherCharSets : false);");
out.println("				} );");
out.println("		</script>");
out.println("		 <style type='text/css'>");
out.println("      body {");
out.println("        padding-top: 40px;");
out.println("        padding-bottom: 40px;");
out.println("        background-color: #f5f5f5;");
out.println("      }");
out.println("      .form-signin {");
out.println("        max-width: 300px;");
out.println("        padding: 19px 29px 29px;");
out.println("        margin: 0 auto 20px;");
out.println("        background-color: #fff;");
out.println("        border: 1px solid #e5e5e5;");
out.println("        -webkit-border-radius: 5px;");
out.println("           -moz-border-radius: 5px;");
out.println("                border-radius: 5px;");
out.println("        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);");
out.println("           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);");
out.println("                box-shadow: 0 1px 2px rgba(0,0,0,.05);");
out.println("      }");
out.println("      .form-signin .form-signin-heading,");
out.println("      .form-signin .checkbox {");
out.println("        margin-bottom: 10px;");
out.println("      }");
out.println("      .form-signin input[type='text'],");
out.println("      .form-signin input[type='password'] {");
out.println("        font-size: 16px;");
out.println("        height: auto;");
out.println("        margin-bottom: 15px;");
out.println("        padding: 7px 9px;");
out.println("      }");
out.println("    </style>");
out.println("  </head>");
out.println("  <body>");
out.println("	<div class='container'>");
out.println("	  <!-- Admin Section -->");
out.println("    <form class='form-signin' action='views' method='post'>");
out.println("        <h3 class='form-signin-heading'>Admin Login:</h3>");

out.println("<div class='control-group error'>");
out.println("  <div class='controls'>");
out.println("    <input type='text' name='user_name' class='input-block-level' placeholder='Username' id='uname'>");
out.println("    <span class='help-inline'>No such user name</span>");
out.println("  </div>");
out.println("</div>");
out.println("        <input type='password' name='password' class='input-block-level' placeholder='Password' id='pword'>");
out.println("		<button class='btn btn-primary' type='submit'>Sign in</button>");
out.println("  </form>");
out.println("	<form class='form-signin' action='views' method='post'>");
out.println("	  <!-- Teacher Section -->");
out.println("		<h3 class='form-signin-heading'>Session Search:</h3>");
out.println("		<input type='text' class='input-block-level' name='sess_id' id='session_id' placeholder='Session ID'>");
out.println("		<button class='btn btn-primary' type='submit'>Search</button>");
out.println("      </form>");
out.println("    </div> <!-- /container -->  ");
out.println("  </body>");
out.println("</html>");
         	

         }
         //if there is one, back up one step and check to see if any of those user names have a password that equals what was entered
         else{
         rset.previous();
         boolean valid = false;
         while(rset.next()){
         	if (rset.getString("pword").equals(password))
         		valid = true;
         }
         //if no valid password, output the start page stating you have an invalid password
         if (valid == false){
         	out.println("<!DOCTYPE HTML>");
         	         	out.println("<html>");
out.println("  <head>");
out.println("	<title>Main Page</title>");
out.println("	    <script src='./js/jquery.js'></script>");
out.println("		<script src='./libraries/bootstrap/js/bootstrap.min.js'></script>");
out.println("		<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap.min.css'>");
out.println("		<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap-responsive.min.css'>");
out.println("		<script type=\"text/javascript\" language=\"javascript\" src=\"./libraries/AlphaNum/jquery.alphanum.js\"></script>");
out.println("		<script type=\"text/javascript\" charset=\"utf-8\">");
out.println("		$(document).ready(function() {");
out.println("			$(\"#uname\").alphanum({allowOtherCharSets : false});");
out.println("			$(\"#pword\").alphanum({allowOtherCharSets : false});");
out.println("			$(\"#session_id\").alphanum(allowLatin: false, allowOtherCharSets : false);");
out.println("				} );");
out.println("		</script>");
out.println("		 <style type='text/css'>");
out.println("      body {");
out.println("        padding-top: 40px;");
out.println("        padding-bottom: 40px;");
out.println("        background-color: #f5f5f5;");
out.println("      }");
out.println("      .form-signin {");
out.println("        max-width: 300px;");
out.println("        padding: 19px 29px 29px;");
out.println("        margin: 0 auto 20px;");
out.println("        background-color: #fff;");
out.println("        border: 1px solid #e5e5e5;");
out.println("        -webkit-border-radius: 5px;");
out.println("           -moz-border-radius: 5px;");
out.println("                border-radius: 5px;");
out.println("        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);");
out.println("           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);");
out.println("                box-shadow: 0 1px 2px rgba(0,0,0,.05);");
out.println("      }");
out.println("      .form-signin .form-signin-heading,");
out.println("      .form-signin .checkbox {");
out.println("        margin-bottom: 10px;");
out.println("      }");
out.println("      .form-signin input[type='text'],");
out.println("      .form-signin input[type='password'] {");
out.println("        font-size: 16px;");
out.println("        height: auto;");
out.println("        margin-bottom: 15px;");
out.println("        padding: 7px 9px;");
out.println("      }");
out.println("    </style>");
out.println("  </head>");
out.println("  <body>");
out.println("	<div class='container'>");
out.println("	  <!-- Admin Section -->");
out.println("    <form class='form-signin' action='views' method='post'>");
out.println("        <h3 class='form-signin-heading'>Admin Login:</h3>");


out.println("    <input type='text' name='user_name' class='input-block-level' placeholder='Username' id='uname'>");
out.println("<div class='control-group error'>");

out.println("  <div class='controls'>");
out.println("        <input type='password' name='password' class='input-block-level' placeholder='Password' id='pword'>");
out.println("    <span class='help-inline' style='padding-top:0px;'>Invalid password</span>");
out.println("  </div>");
out.println("</div>");
out.println("		<button class='btn btn-primary' type='submit'>Sign in</button>");
out.println("  </form>");
out.println("	<form class='form-signin' action='views' method='post'>");
out.println("	  <!-- Teacher Section -->");
out.println("		<h3 class='form-signin-heading'>Session Search:</h3>");
out.println("		<input type='text' class='input-block-level' name='sess_id' id='session_id' placeholder='Session ID'>");
out.println("		<button class='btn btn-primary' type='submit'>Search</button>");
out.println("      </form>");
out.println("    </div> <!-- /container -->  ");
out.println("  </body>");
out.println("</html>");
         }
         else{
		//otherwise, login as an admin
		
		/*
		*	This is the Admin Page that will only print if login credentials match DB
		*/
		out.println("<!DOCTYPE HTML>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>");
		         out.println("<style type=\"text/css\" title=\"currentStyle\">");
		out.println("	@import \"./libraries/TableTools-2.1.5/media/css/TableTools.css\";");
		out.println("@import \"./libraries/DataTables-1.9.4/media/css/demo_page.css\";");
		out.println("</style>");
		out.println("<script src='./js/jquery.js'></script>");
		out.println("<script src='./libraries/DataTables-1.9.4/media/js/jquery.dataTables.js'></script>");
		out.println("<link rel='stylesheet' type='text/css' href='./libraries/DataTables-1.9.4/media/css/jquery.dataTables.css'>");
		out.println("<link rel='stylesheet' type='text/css' href='./libraries/DataTables-1.9.4/media/css/demo_table.css'>");
		out.println("<script type=\"text/javascript\" charset=\"utf-8\" src=\"./libraries/TableTools-2.1.5/media/js/TableTools.js\"></script>");
		 out.println("<script type=\"text/javascript\" charset=\"utf-8\" src=\"./libraries/TableTools-2.1.5/media/js/ZeroClipboard.js\"></script>");
		out.println("<script src='./libraries/bootstrap/js/bootstrap.min.js'></script>");
		out.println("<script type=\"text/javascript\" language=\"javascript\" src=\"./libraries/AlphaNum/jquery.alphanum.js\"></script>");
		out.println("<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap.min.css'>");
		out.println("<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap-responsive.min.css'>");
		 
		 //CSS Style for body
		out.println("<style>");
		out.println(" body {");
		out.println("padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */} .datatable-scroll {clear:both;width:100%;overflow:auto;}</style>");
		//ajax that gets the session table from sessionrequest.java
		out.println("<script>");
		out.println("function loadDB(){");
		out.println("  $.ajax({");
		out.println("    url:'sessionreq',");
		out.println("    type:'GET',");
		out.println("     success: function(data) {");
		out.println("             $('#ajax_result').html(data);");
		out.println("			 $('#session_table').dataTable();");
		out.println("         },");
		out.println("         error: function(xhr, status, error) {");
		out.println("           $('#ajax_result').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
		out.println("         }");
		out.println("  })");
		out.println("}");
		//automatically execute ajax for session table on page load
		out.println("$(document).ready(function(){");
		out.println("    loadDB();");
		out.println("});");
		out.println("</script>");
		out.println("<script>");
		//ajax for getting a student table based on session id, takes a sessionid argument from the text box #sess using adminrequest.java
		out.println("function loadDB2(){");
		out.println("  $.ajax({");
		out.println("    url:'adminreq',");
		out.println("    data: {sessionid: $('#sess').val()},");
		out.println("    type:'GET',");
		out.println("     success: function(data) {");
		out.println("             $('#ajax_result2').html(data);");
		out.println("			 $('#students_table').dataTable();");
		out.println("         },");
		out.println("         error: function(xhr, status, error) {");
		out.println("           $('#ajax_result2').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
		out.println("         }");
		out.println("  })");
		out.println("}");
		//function for the button #b1 to execute the ajax that gets a student table based on sessionid
		out.println("$(document).ready(function(){");
		out.println("    $('#b1').click(function(){");
		out.println("    loadDB2();");
		out.println("      });");
		out.println("});");
		out.println("</script>");
		out.println("<script>");
		//ajax to create a session, takes the boxes #fnam, #lan, #mai, #phon to create a session using addrequest.java
		out.println("function loadDB3(){");
		out.println("  $.ajax({");
		out.println("    url:'addreq',");
		out.println("    data: {fname: $('#fnam').val(), lname: $('#lnam').val(), email: $('#emai').val(), phone: $('#phon').val()},");
		out.println("    type:'GET',");
		out.println("     success: function(data) {");
		out.println("             $('#ajax_result3').html(data);");
		out.println("             loadDB();");
		out.println("         },");
		out.println("         error: function(xhr, status, error) {");
		out.println("           $('#ajax_result3').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
		out.println("         }");
		out.println("  })");
		out.println("}");
		//use the alphanum extension to limit the inputs when creating a session, or entering a session number to search
		out.println("$(document).ready(function(){");
		out.println("  $(\"#fnam\").alpha();");
		out.println("  $(\"#lnam\").alpha();");
		out.println("  $(\"#emai\").alphanum({allow: '.@_'});");
		out.println("  $(\"#phon\").alphanum({allow: '-()', allowLatin: false});");
		out.println("  $(\"#sess\").numeric();");
		//create the click function for the button that creates a session	
		out.println("    $('#b2').click(function(){");
		out.println("    loadDB3();");
		                  
		out.println("      });");
		out.println("});");
		out.println("</script>");
		out.println("<title>Admin</title>");
		out.println("</head>");
		out.println("<body>");
		
		//Bootstrap Fixed Navbar
		 out.println("<div class='navbar navbar-inverse navbar-fixed-top'>");
			out.println("<div class='navbar-inner'>");
				out.println("<div class='container'>");
				out.println("<button type='button' class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>");
            out.println("<span class='icon-bar'></span>");
            out.println("<span class='icon-bar'></span>");
            out.println("<span class='icon-bar'></span>");
          out.println("</button>");
          out.println("<a class='brand' href=\"NatureHome.html\">Cubic Meter Community</a>");
          out.println("<div class='nav-collapse collapse'>");
            out.println("<ul class='nav'>");
              out.println("<li class='active'><a href='#'>Administrator</a></li>");
              out.println("<li><a href='#about'>About</a></li>");
              out.println("<li><a href=\"Email_page.html\">Contact Support</a></li>");
            out.println("</ul>");
          out.println("</div><!--/.nav-collapse -->");
		  out.println("<input type='button' class='btn btn-danger' style='float:right;' value='Logout' onClick=\"location.href='./NatureHome.html';\"/>");
        out.println("</div>");
      out.println("</div>");
    out.println("</div>");
	
	//Wrapper for session Table
	out.println("<div class='container'>");
		out.println("<h2>Sessions:</h2>");
		out.println("<div id='ajax_result'></div><br>");
		out.println("<div class=\"form-horizontal\">");
		out.println("  Search: <input type='text' name='search_text' id='sess' maxlength=\"7\">");
		out.println("  <input type='button' value='Search' class='btn btn-primary' name='search_button' id='b1'>");
		out.println("</div>");
		//wrapper for student table
		out.println("<div id='ajax_result2' style='width:100%;'></div><br>");
		out.println("<div class=\"form-horizontal\">");
		out.println("<h2>Create Session:</h2>");
		//create session boxes and buttons
		out.println("First Name: <input type='text' name='fname' id='fnam' maxlength='20'/><br><br>");
		out.println("Last Name: <input type='text' name='lname' id='lnam' maxlength='20'/><br><br>");
		out.println("Email: <input type='text' name='email' id='emai' maxlength='20'/><br><br>");
		out.println("Phone: <input type='text' name='phone' id='phon' maxlength='20'/><br><br>");
		out.println("<input type='button' class='btn btn-primary' value='Create Session' name='crt_sess' id='b2'>");
		out.println("<!--End test rows in table-->");
		out.println("</div>");
		
		
	out.println("</div> <!-- /container -->");
	
	out.println("</body></html>");
	//END ADMIN PAGE

	/******************************************************************************************************
	* TEACHER SEARCH FOR SESSION
	******************************************************************************************************/
	}
	//if there WAS a sessionid, then you tried to login as a teacher
	  }}else{
		/*
		* Generate HTML code for the TeacherPage.html page
		*/
		out.println("<!DOCTYPE HTML>");
		out.println("<html>");
         out.println("<head>");
         out.println("<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>");
         out.println("<style type=\"text/css\" title=\"currentStyle\">");
		out.println("	@import \"./libraries/TableTools-2.1.5/media/css/TableTools.css\";");
		out.println("@import \"./libraries/DataTables-1.9.4/media/css/demo_page.css\";");
		out.println("</style>");
		 out.println("<script src='./js/jquery.js'></script>");
		 out.println("<script src='http://code.highcharts.com/highcharts.js'></script>");
		 out.println("<script src='http://code.highcharts.com/modules/exporting.js'></script>");
		 out.println("<script src='./libraries/DataTables-1.9.4/media/js/jquery.dataTables.js'></script>");

		 out.println("<link rel='stylesheet' type='text/css' href='./libraries/DataTables-1.9.4/media/css/jquery.dataTables.css'>");
		 out.println("<link rel='stylesheet' type='text/css' href='./libraries/DataTables-1.9.4/media/css/demo_table.css'>");
		 		 out.println("<script type=\"text/javascript\" charset=\"utf-8\" src=\"./libraries/TableTools-2.1.5/media/js/TableTools.js\"></script>");
		 out.println("<script type=\"text/javascript\" charset=\"utf-8\" src=\"./libraries/TableTools-2.1.5/media/js/ZeroClipboard.js\"></script>");
		 out.println("<script src='./js/charts.js'></script>");

		 out.println("<script src='./libraries/bootstrap/js/bootstrap.min.js'></script>");
		 out.println("<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap.min.css'>");
		 out.println("<link rel='stylesheet' type='text/css' href='./libraries/bootstrap/css/bootstrap-responsive.min.css'>");
		 
		 //CSS Style for body
		 out.println("<style>");
			out.println("body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */} ");
			out.println(".datatable-scroll {clear:both;width:100%;overflow:auto;} ");
			// out.println("div{border:1px solid red} ");
			out.println(".span4{border:.5px solid black} ");
		out.println("</style>");

         out.println("<script>");
         //Ajax that uses teacherrequest.java to get a student table, takes the session id from the main page as argument
         out.println("function loadDB(){");
            out.println("$.ajax({");
               out.println("url:'teacherreq',");
               out.println("data: {sessionid: " + session_id + "},");
               out.println("type:'GET',");
                out.println("success: function(data) {");
                      out.println("$('#ajax_result').html(data);");
					  out.println("$('#students_table').dataTable();");
					  //Used for making the charts after the table is fetched  
					  out.println("temp_chart();");
					  out.println("air_pressure_chart();");
					  out.println("humidity_chart();");
					  out.println("wind_speed_chart();");
					  out.println("rain_meter_chart();");
					  out.println("altitude_chart();");
					  out.println("ph_chart();");
					  
                  out.println("},");
                 out.println("error: function(xhr, status, error) {");
                   out.println("$('#ajax_result').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
                out.println("}");
            out.println("})");
         out.println("}");

         //automatically execute ajax for student table on page load
         out.println("$(document).ready(function(){");
              out.println("loadDB();");
         out.println("});");
         out.println("</script>");
         out.println("<title>Teacher Page</title>");
         out.println("</head>");
         out.println("<body>");
		 
		 //Bootstrap Fixed Navbar
		 out.println("<div class='navbar navbar-inverse navbar-fixed-top'>");
			out.println("<div class='navbar-inner'>");
				out.println("<div class='container'>");
				out.println("<button type='button' class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>");
            out.println("<span class='icon-bar'></span>");
            out.println("<span class='icon-bar'></span>");
            out.println("<span class='icon-bar'></span>");
          out.println("</button>");
          out.println("<a class='brand' href=\"NatureHome.html\">Cubic Meter Community</a>");
          out.println("<div class='nav-collapse collapse'>");
            out.println("<ul class='nav'>");
              out.println("<li class='active'><a href='#'>Teacher</a></li>");
              out.println("<li><a href='#about'>About</a></li>");
              out.println("<li><a href=\"Email_page.html\">Contact Support</a></li>");
            out.println("</ul>");
          out.println("</div><!--/.nav-collapse -->");
		  out.println("<input type='button' class='btn btn-danger' style='float:right;' value='Logout' onClick=\"location.href='./NatureHome.html';\"/>");
        out.println("</div>");
      out.println("</div>");
    out.println("</div>");

	//Begin Wrapper for Table
	out.println("<div class='container'>");
		 out.println("<div id='ajax_result'></div>");
         out.println("</body>");
         out.println("</html>");
         out.println("<br><br>");
         //divs for charts
         out.println("<div id='charts_div' class='row'>");
			out.println("<div id='temp_chart' class='row'></div>");
			out.println("<div id='pressure_chart' class='row'></div>");
			out.println("<div id='humidity_chart' class='row'></div>");
			out.println("<div id='wind_speed_chart' class='row'></div>");
			out.println("<div id='rain_meter_chart' class='row'></div>");
			out.println("<div id='altitude_chart' class='row'></div>");
			out.println("<div id='ph_chart' class='row'></div>");
		out.println("</div>");
	out.println("</div> <!-- /container -->");

         out.println("</body></html>");
	  }
      } 
      catch (Exception e) 
         {
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
         } 
   }
}