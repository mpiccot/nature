
import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
 //Servlet that requests a session list
public class SessionRequest extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      String firstName = "";
      try {

         //connect to the DB
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         String gquery = "select * from discovery_sessions";
         ResultSet rset = stmt.executeQuery(gquery);
         //variables used for editing
 out.println("<script type='text/javascript' charset='utf-8'>");
      out.println("  var oTable2;");
      out.println("  var giRedraw2 = false;");
      out.println("  var nEditing2 = false");
      out.println("  var rowedit2 = null");

      //ajax to edit the session table, takes the row information as an argument and calls editreq.java
      out.println("function editDB2(){");
      out.println("  var dat2 = oTable2._(rowedit2);");
      out.println("  $.ajax({");
      out.println("    url:'editreq',");
      out.println("    data: {data: dat2.toString(), type: 'session'},");
      out.println("    type:'GET',");
      out.println("     success: function(data) {");
      out.println("             $('#ajax_result5').html(data);");
      out.println("         },");
      out.println("         error: function(xhr, status, error) {");
      out.println("           $('#ajax_result5').html('<p>AJAX ERROR</p> ' +' ' + xhr.responseText + ' ' + status + error);");
      out.println("         }");
      out.println("  })");
      out.println("}");
      out.println("  $(document).ready(function() {");
            // Add a click handler to the rows this is what highlights rows for selection
      out.println("     $('#session_table tbody').click(function(event) {");
      out.println("        $(oTable2.fnSettings().aoData).each(function (){");
      out.println("           $(this.nTr).removeClass('row_selected');");
      out.println("        });");
      out.println("        $(event.target.parentNode).addClass('row_selected');");
      out.println("     });");
         //edit function for the edit button
         //if you are not in edit mode, call editrow on the currently selected row, and change edit button to save
         //if you are in edit mode, call saverow on the edited row, then execute the editDB ajax save, and change save button back to edit
      out.println("$('#edit2').click( function () {");
      out.println("        var anSelected2 = fnGetSelected2( oTable2 );");   
     out.println("if(nEditing2 == false && anSelected2[0] !== null){");
      out.println("editRow2( oTable2, anSelected2[0]);");
      out.println("nEditing2 = true;");
      out.println("rowedit2 = anSelected2[0]");
      out.println("document.getElementById(\"edit2\").value=\"Save Modified Session\";");
      out.println("}");
      out.println("else if (nEditing2 == true){");
      out.println("saveRow2( oTable2, rowedit2 );");
      out.println("editDB2();");
      out.println("document.getElementById(\"edit2\").value=\"Edit Selected Session\";");
      out.println("nEditing2 = false;");
      out.println("}");
      out.println("} );");
            
            // Init the datatable 
      out.println("     oTable2 = $('#session_table').dataTable({");
                  out.println("           'bPaginate': false,");
                  out.println("\"sDom\": 'T<\"clear\">r<\"H\"lf><\"datatable-scroll\"t><\"F\"ip>',");
                  out.println("\"oTableTools\": {");
                  out.println("\"sSwfPath\": \"./libraries/TableTools-2.1.5/media/swf/copy_csv_xls_pdf.swf\"");
                  out.println("},");
                  out.println("           'bPaginate': true,");
                  out.println("           'bRetrieve': true");
      out.println("  } );");
         out.println("} );");
         
         // Get the rows which are currently selected, and returns them
      out.println("  function fnGetSelected2( oTableLocal2 )");
      out.println("  {");
      out.println("     var aReturn2 = new Array();");
      out.println("     var aTrs2 = oTableLocal2.fnGetNodes();");
      
      out.println("     for ( var i2=0 ; i2<aTrs2.length ; i2++ )");
      out.println("     {");
      out.println("        if ( $(aTrs2[i2]).hasClass('row_selected') )");
      out.println("        {");
      out.println("           aReturn2.push( aTrs2[i2] );");
      out.println("        }");
      out.println("     }");
      out.println("     return aReturn2;");
      out.println("  }");
   //get datafunction that gets the row data from the selected row in the table
      out.println("  function getData2( oTableLocal2 )");
      out.println("  {");
      out.println("     var data2 = oTableLocal2._(fnGetSelected2(oTableLocal2))[0];");
      out.println("     return data2[0];");
      out.println("  }");

      //saverow2 updates the html in the table with the new edited values
      out.println("function saveRow2 ( oTableLocal2, nRow2 )");
      out.println("{");
      out.println("var jqInputs2 = $('input', nRow2);");
      out.println("oTableLocal2.fnUpdate( jqInputs2[0].value, nRow2, 1, false );");
      out.println("oTableLocal2.fnUpdate( jqInputs2[1].value, nRow2, 2, false );");
      out.println("oTableLocal2.fnUpdate( jqInputs2[2].value, nRow2, 3, false );");
      out.println("oTableLocal2.fnUpdate( jqInputs2[3].value, nRow2, 4, false );");
      out.println("oTableLocal2.fnUpdate( jqInputs2[4].value, nRow2, 5, false );");
      out.println("oTableLocal2.fnDraw();");
      out.println("}");
      //editrow2 changes each value in the selected row with a textbox with the initial value set to be the value before editing
     out.println("function editRow2 ( oTableLocal2, nRow2 )");
      out.println("{");
      out.println("var aData2 = oTableLocal2.fnGetData(nRow2);");
      out.println("var jqTds2 = $('>td', nRow2);");
      out.println(" jqTds2[1].innerHTML = '<input type=\"text\" value=\"'+aData2[1]+'\" id=\"sebox1\" maxlength=\"20\">';");
      out.println(" jqTds2[2].innerHTML = '<input type=\"text\" value=\"'+aData2[2]+'\" id=\"sebox2\" maxlength=\"20\">';");
      out.println(" jqTds2[3].innerHTML = '<input type=\"text\" value=\"'+aData2[3]+'\" id=\"sebox3\" maxlength=\"20\">';");
      out.println(" jqTds2[4].innerHTML = '<input type=\"text\" value=\"'+aData2[4]+'\" id=\"sebox4\" maxlength=\"20\">';");
      out.println(" jqTds2[5].innerHTML = '<input type=\"text\" value=\"'+aData2[5]+'\" id=\"sebox5\" maxlength=\"1\">';");
      out.println("  $(\"#sebox1\").alphanum({allowOtherCharSets: false, allowNumeric: false});");
      out.println("  $(\"#sebox2\").alphanum({allowOtherCharSets: false, allowNumeric: false});");
      out.println("  $(\"#sebox3\").alphanum({allow: '.@_', allowOtherCharSets : false});");
      out.println("  $(\"#sebox4\").alphanum({allow: '-()', allowLatin: false, allowOtherCharSets : false});");
      out.println("  $(\"#sebox5\").alphanum({allowLatin: false, allowNumeric: false, allow: 'YN', allowOtherCharSets : false});");
      out.println("}");
      out.println("</script>");
      //create the session table using a loop on the DB result set
      out.println("<table id= 'session_table' border='1' class='display'><thead><tr><th>Session ID</th><th>Last Name</th><th>First Name</th><th>E-mail</th><th>Phone</th><th>Active</th><th>Creation Date</th></tr></thead><tbody>");

         while(rset.next()){
            out.println("<tr><td>" + rset.getString("SESSION_NUMBER") + "</td><td>" + rset.getString("PROF_LNAME") + "</td><td>" + rset.getString("PROF_FNAME") + "</td><td>" + rset.getString("PROF_EMAIL") + "</td>");
            out.println("<td>" + rset.getString("PROF_PHONE") + "</td><td>" + rset.getString("SESSION_ACTIVE") + "</td><td>" + rset.getString("CREATION_DATE") + "</td>");
            out.println("</tr>");
         }
		 out.println("</tbody></table><br>");
       out.println("<input type='button' class='btn btn-primary' value='Edit Selected Session' name='edit_sess' id='edit2'>");
      } 
      catch (Exception e) 
         {
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
         } 
         finally 
         {
            out.close();
            try 
            {
               if (conn != null) 
               {
                  conn.close();
               }
            } 
            catch (SQLException e)
            {
            }               
         }
   }
}