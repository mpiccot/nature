import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
 //Servlet for the teacher requesting a student table
public class TeacherRequest extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      Connection conn = null;
      Statement stmt = null;
      try {
         //connect to the DB
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/nature", "cmcadmin", "CNM!cmc1");
         stmt = conn.createStatement();
         //make the query with the sessionid parameter, and execute
         String gquery = "select * from student where sessionID=" + request.getParameter("sessionid");//request.getParameter("sessionid");
         ResultSet rset = stmt.executeQuery(gquery);

         out.println("<script type='text/javascript' charset='utf-8'>");
         out.println("  $(document).ready(function() {");
         // Init the datatable 
         out.println("     $('#students_table').dataTable({");
         out.println("           'bPaginate': true,");
         out.println("\"sDom\": 'T<\"clear\">r<\"H\"lf><\"datatable-scroll\"t><\"F\"ip>',");
         out.println("\"oTableTools\": {");
         out.println("\"sSwfPath\": \"./libraries/TableTools-2.1.5/media/swf/copy_csv_xls_pdf.swf\"");
         out.println("},");       
         out.println("           'bRetrieve': true");
         out.println("  } );");
         out.println("} );");
         out.println("</script>");
        //scan through DB result set and create an html table using a loop
        if (rset.next()){
            out.println("<h1>SessionID: " + rset.getString("sessionid") + "</h1>");
            out.println("<table id='students_table' border='1' class='display'><thead><tr><th>Student ID</th><th>Last Name</th><th>First Name</th><th>Cubic Meter</th><th>Temperature</th><th>Air Pressure</th><th>Humidity</th><th>Wind Speed</th><th>Rain Meter</th><th>Current Precipitation</th><th>Altitude</th><th>Cloud Type</th><th>Cloud Cover</th><th>Insects</th><th>Soil pH</th><th>Soil Color</th><th>Soil Type</th><th>Canopy Cover</th></tr></thead><tbody>");
            rset.previous();
         while(rset.next()){
            out.println("<tr><td>" + rset.getString("ID") + "</td><td>" + rset.getString("lName") + "</td><td>" + rset.getString("fName") + "</td>");
            out.println("<td>" + rset.getString("cubicNum") + "</td><td>" + rset.getString("temperature") + "</td><td>" + rset.getString("aPressure") + "</td>");
            out.println("<td>" + rset.getString("humidity") + "</td><td>" + rset.getString("wSpeed") + "</td><td>" + rset.getString("rMeter") + "</td>");
            out.println("<td>" + rset.getString("cPrecipitation") + "</td><td>" + rset.getString("altitude") + "</td><td>" + rset.getString("cType") + "</td>");
            out.println("<td>" + rset.getString("cCover") + "</td><td>" + rset.getString("insects") + "</td><td>" + rset.getString("sPH") + "</td>");
            out.println("<td>" + rset.getString("sColor") + "</td><td>" + rset.getString("sType") + "</td><td>" + rset.getString("canopyCover") + "</td>");
            out.println("</tr>");
            
         }
		 out.println("</tbody></table>");
      }
      else{
         //if you didn't find a sessionid, just state that no session id was found, and send back after 6 seconds
         out.println("<p>No such sessionID found/no records for this session.</p>");
         out.println("<p>You will be sent back in 6 seconds.</p>");
         out.println("<META HTTP-EQUIV=\"refresh\" CONTENT=\"6;URL=./NatureHome.html\">");
      }
      out.println("<div id='ajax_result3' style='width:100%;'></div>");
      } 
      catch (Exception e) 
         {
            e.printStackTrace();
            System.err.println("Exception: " + e.getMessage());
            //this should be caught before you can enter it, but just in case...
            out.println("Invalid entry, only use the number 0-9 for your session ID");
         } 
         finally 
         {
            out.close();
            try 
            {
               if (conn != null) 
               {
                  conn.close();
               }
            } 
            catch (SQLException e)
            {
            }               
         }
   }
}