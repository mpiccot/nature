function get_table_nodes(){
	var oTable = $('#students_table').dataTable();
	
	//get all row nodes from dataTable
	var nNodes = oTable.fnGetNodes();
	var students = [];
	var temperature = [];
	var air_pressure = [];
	var humidity = [];
	var wind_speed = [];
	var rain_meter = [];
	var altitude = [];
	var soil_ph = [];
	
	for(var i=0;i<nNodes.length;i++){
		var data = oTable.fnGetData(nNodes[i]);
		
		students.push(data[2] + " " + data[1]);
		temperature.push(parseFloat(data[4]));
		air_pressure.push(parseFloat(data[5]));
		humidity.push(parseFloat(data[6]));
		wind_speed.push(parseFloat(data[7]));
		rain_meter.push(parseFloat(data[8]));
		altitude.push(parseFloat(data[10]));
		soil_ph.push(parseFloat(data[14]));
	}
	
	// alert(students );
	// alert(temperature );
	// alert(air_pressure );
	// alert(humidity );
	// alert(wind_speed );
	// alert(rain_meter );
	// alert(altitude );
	// alert(soil_ph );
	
	var tableJSON = {Students: students, Temperature:temperature, Air_Pressure:air_pressure,
		Humidity:humidity, Wind_Speed:wind_speed, Rain_Meter:rain_meter, Altitude:altitude, Soil_PH:soil_ph
	};
	
	return tableJSON;
}

function temp_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#temp_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Temperature Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 110,
                title: {
                    text: 'Degrees (F)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Degrees </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Temperature',
                data: tableJSON.Temperature
    
            }]
        });
    });
}

function air_pressure_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#pressure_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Air Pressure Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 1500,
                title: {
                    text: 'PSI'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} PSI </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Air Pressure',
                data: tableJSON.Air_Pressure
    
            }]
        });
    });
	
}

function humidity_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#humidity_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Humidity Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'Humidity'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Humidity </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Air Pressure',
                data: tableJSON.Humidity
    
            }]
        });
    });
	
}

function wind_speed_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#wind_speed_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Wind Speed Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 50,
                title: {
                    text: 'Mph'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Mph </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Air Pressure',
                data: tableJSON.Wind_Speed
    
            }]
        });
    });
	
}

function rain_meter_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#rain_meter_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Rain Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 10,
                title: {
                    text: 'Inches'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Inches </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Rainfall',
                data: tableJSON.Rain_Meter
    
            }]
        });
    });
	
}
function altitude_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#altitude_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Altitude Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'FEET above sea level'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} FEET above sea level </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Altitude',
                data: tableJSON.Altitude
    
            }]
        });
    });
	
}

function ph_chart(){
//Create JSON object for all table data
var tableJSON = get_table_nodes();
	$(function () {
        $('#ph_chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'pH Recorded'
            },
            subtitle: {
                text: 'Cubic Meter Community'
            },
            xAxis: {
                categories: tableJSON.Students
            },
            yAxis: {
                min: 0,
                max: 15,
                title: {
                    text: 'pH'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} pH </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'pH',
                data: tableJSON.Soil_PH
    
            }]
        });
    });
	
}